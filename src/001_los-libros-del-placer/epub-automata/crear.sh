pc-automata -f ../archivos-madre/md/todo.md \
            -n ../archivos-madre/md/notas.md \
            -c ../archivos-madre/img/portada.jpg \
            -i ../archivos-madre/img/ \
            -s ../../../template/css/styles.css \
            --reset \
            --compress \
            --no-ace \
            --overwrite
